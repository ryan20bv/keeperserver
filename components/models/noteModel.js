const mongoose = require( "mongoose" );
const Schema = mongoose.Schema;

const notesSchema = new Schema( {
  id:String,
	title: String,
	content: String,
});

module.exports = mongoose.model( "Note", notesSchema );