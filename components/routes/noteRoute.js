const express = require( "express" );
const NoteRouter = express.Router();
const NoteModel = require( "../models/noteModel.js" );

NoteRouter.get( "/", async ( req, res ) => {
  const DBnotes = await NoteModel.find();
  res.send(DBnotes)
})

NoteRouter.post( "/createNote", async ( req, res ) => {
  try {
    let newNote = new NoteModel( {
      id:req.body.id,
      title: req.body.title,
      content: req.body.content
    } )
    newNote = await newNote.save();
    res.send( "Added Success" );
  } catch (error) {
    console.log(error);
  }
} )

NoteRouter.delete( "/deleteNote",  async (req,res) => {
  try {
    await NoteModel.findOneAndDelete({id:req.body.id})
    res.send("deleted Successfully")
  } catch ( error ) {
    console.log("here deleted")
    console.log(error);
  }
})

NoteRouter.patch( "/updateNote", async ( req, res ) => {
  try {
    const updates = {
      title: req.body.title,
      content: req.body.content
    }
    await NoteModel.findOneAndUpdate( {id: req.body.id }, updates, { new: true } )
    res.send("update Success");

  } catch (error) {
    console.log(error);
  }
})


module.exports = NoteRouter;