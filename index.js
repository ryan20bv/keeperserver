require("dotenv").config();
const express = require("express");
const mongoose = require( "mongoose" );
const cors = require( "cors" );

const app = express();
const env = process.env;

app.use( cors() );
app.use(express.urlencoded({ extended: true }));
app.use(express.json());



mongoose
  .connect( env.DB_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  } )
	.then(() => {
		try {
			console.log("Server connected to DATABASE");
    } catch ( error ) {
      console.log("mongoose error")
			console.log(error);
		}
	});



app.listen(env.PORT, () => {
	console.log(`Keeper listening to port ${env.PORT}`);
});

//routes

const notesRouter = require( "./components/routes/noteRoute.js" )
app.use( "/", notesRouter );